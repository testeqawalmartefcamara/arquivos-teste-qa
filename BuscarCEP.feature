# language: pt
Funcionalidade: Buscar dados de CEP nos correios
  Com um usuário do ecommerce do Wallmart
  Para testar a busca de dados por CEP
  Os dados devem ser retornado para o ecommerce do Wallmart

  Cenário: Buscar CEP
    Dado a URL:
    | http://correiosapi.apphb.com/cep/05345000 |
    | http://correiosapi.apphb.com/cep/69900900 |
    Quando efetuar o request
    Então deve ser devolvido o CEP igual a <cep>
    E o tipo logradouro igual a <TipoLogradouro>
    E o logradouro igual a <logradouro>
    E o bairro igual a <bairro>
    E a cidade igual a <cidade>
    E o estado igual a <estado>

    Exemplos:
      | Cep      | TipoLogradouro | logradouro                 | bairro  | cidade     | estado |
      | 05345000 | Avenida        | Miguel Frias e Vasconcelos | Jaguaré | São Paulo  | SP     |
      | 69900900 | Avenida        | Getúlio Vargas, 232        | Centro  | Rio Branco | AC     |
 
  Cenário: Buscar CEP
    Dado a URL:
    | http://correiosapi.apphb.com/cep/123456789 |
    | http://correiosapi.apphb.com/cep/1 |
    Quando efetuar o request
    Então deve ser devolvido a mensagem "Endereço não encontrado!"

  Cenário: Buscar CEP
    Dado a URL:
    | http://correiosapi.apphb.com/cep/ |
    | http://correiosapi.apphb.com/cep/05345-000 |
    | http://correiosapi.apphb.com/cep/05345 000 |
    Quando efetuar o request
    Então deve ser devolvido a mensagem "The resource cannot be found."