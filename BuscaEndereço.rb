class ViaCepWebService
  include HTTParty

  base_uri "correiosapi.apphb.com"

  attr_accessor :cep

  def initialize(cep)
    @cep = cep
  end

  def result
    @result ||= self.class.get("/cep/#{cep}").parsed_response
  end

  def valid?
    result.is_a?(Hash) && result["erro"].blank?
  end

  def address
    if valid?
      {
        cep:              result["cep"].strip,
        tipoDeLogradouro: result["tipoDeLogradouro"].strip,
        logradouro:       result["logradouro"].strip,
        bairro:           result["bairro"] || "",
        cidade:           result["cidade"] || "",
        estado:           result["estado"] || "",

      }
    else
      {}
    end
  end
  
end