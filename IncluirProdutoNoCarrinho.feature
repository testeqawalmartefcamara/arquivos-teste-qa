# language: pt
Funcionalidade: Incluir o Produto no carrinho
  Com um usuário do ecommerce do Wallmart
  Para testar a inclusão do produto no carrinho do ecommerce do Wallmart
  Os usuário do ecommerce do Wallmart devem efetuar adicionar o produto ao carrinho

  Cenário: Adicionar produto no carrinho
    Dado estes usuários:
    | username | password |
    | Teste    | Ghj$#34  |
    | AutTeste | JK88&*%  |
    E ecommerce do Wallmart aberto
    E o usuário logado no ecommerce do Wallmart
    E a tela de busca do ecommerce do Wallmart aberta
    Quando digitar o <produto>
    E clicar no botão em formato de lupa
    E clicar sobre o primeiro produto exibido
    E clicar sobre o botão "Adicionar ao Carrinho"
    E clicar sobre o botão com desenho de um carinho de compras
    Então o produto adicionado ao carrinho deve ser exibido
    E sua descrição deve estar igual ao exibido na seleção
    E seu valor deve estar igual ao exibido na tela do produto

    Exemplos:
      | produto      |
      | TV 32" & 42" |
      | Hot Wheels   |
      | MN9C2BZ/A    |
      | "            |
      | '            |

  Cenário: Adicionar produto no carrinho
    Dado estes usuários:
    | username | password |
    | Teste    | Ghj$#34  |
    | AutTeste | JK88&*%  |
    E ecommerce do Wallmart aberto
    E o usuário logado no ecommerce do Wallmart
    E a tela de busca do ecommerce do Wallmart aberta
    Quando digitar o <produto>
    E clicar no botão em formato de lupa
    E clicar sobre o primeiro produto exibido
    E clicar sobre o botão "Adicionar ao Carrinho"
    E clicar sobre o botão com desenho de um carinho de compras
    E clicar sobre o link remover referente ao produto selecionado
    E digitar o <produto>
    E clicar no botão em formato de lupa
    E clicar sobre o primeiro produto exibido
    E clicar sobre o botão "Adicionar ao Carrinho"
    E clicar sobre o botão com desenho de um carinho de compras
    Então o produto adicionado ao carrinho deve ser exibido
    E sua descrição deve estar igual ao exibido na seleção
    E seu valor deve estar igual ao exibido na tela do produto

    Exemplos:
      | produto      |
      | TV 32" & 42" |
      | Hot Wheels   |
      | MN9C2BZ/A    |
      | "            |
      | '            |