#### Titulo do Projeto ####

O objetivo desse desafio é avaliar o conhecimento dos candidatos e sua capacidade de realiza-lo dentro de um tempo pré-determinado. Não existe resolução certa ou errada, avaliaremos de acordo com o nível de experiência que for exigido pelas vagas disponíveis no momento.
Envie seu desafio mesmo que você não conclua todas as questões, avaliaremos tudo o que for entregue.
Atualmente, utilizamos algumas ferramentas Open Source como padrão em todos os projetos de automação de testes. Queremos saber o quanto você conhece das ferramentas e/ou o quanto você é capaz de aprender sobre elas.
O tempo máximo para envio do link do projeto é de uma semana corrida (claro, envie antes caso termine antes =]).

#### Começando ####

Crie um repositório no GitHub ou no BitBucket para compartilhar o projeto;
Crie um README.md para o projeto, explicando como preparar o ambiente e como executar os testes de cada um dos exercícios;
Use as técnicas e ferramentas que você souber ou aprender durante os estudos, e que considere que sejam adequadas aos exercícios, mesmo que não tenha sido pedido explicitamente. Se achar válido destacar, comente no README.md sobre elas e sobre sua motivação em usá-las. Tudo será avaliado;
A linguagem para realizar o desafio é Ruby.

#### Pré-Requisitos ####

Não houve preparação de ambiente para execução dos testes, devido ao tempo do desafio e falta de conhecimento sobre as linguagens utilizadas.

#### Executando os testes ####

Não houve execução dos testes, devido ao tempo do desafio e falta de conhecimento sobre as linguagens utilizadas.

#### Objetivo dos testes ####

Testar a funcionalidade de adição de produtos no carrinhos.  
Testar a funcionalidade da API dos correios em retornar informações de endereço de CEPs especificos.  

#### Desenvolvimento ####

Feito um desenvolvimento sem testes, não houve depuração, devido ao tempo do desafio e falta de conhecimento sobre as linguagens utilizadas.

#### Construido Com ####

Cucumber  
HTTParty  
Capybara  

#### Versão ####

1.0

#### Autor ####

Wesley Damasceno - Primeiros testes com Cucumber, Capybara e HTTParty.

#### Licensa #####

Open Source

#### Observações ####

Quando digitado 5209 caracteres na busca do Wallmart ela apresenta erro  

Como não tenho experiencia com cucumber, capybara e HTTParty, algumas formas que eu utilizei não seriam melhores praticas, na minha visão, como:  
- Deixar a massa de dados dentro da feature, muito menos usuário é senha, deveria estar em um banco de forma segura;  
- Criar só uma feature para fazer a seleção do produto no carrinho, pois passamos por diversas funcionalidades até chegar na inclusão do produto no carrinho elas não são validadas e podem gerar erro na automação, além de reduzir o reaproveitamento das features para novos testes;  
- Não encontrei testes negativos para a inclusão do produto no carrinho;  
- Não consegui um forma de reaproveitar os stpes dos cenários anteriores, deixando assim um problema de manutenção e não havendo reusabilidade;  
- Com o tempo que foi disponibilizado, não é possivel adquirir conhecimento suficiente para montar um ambiente de desenvolvimento, aprender uma linguagem e suas melhores praticas, sendo assim montei as automações com pesquisas do google, e com certeza seriam muito melhores e funcionais com mais experiência ou tempo para aprendizagem auto didata.  